//Enable use of "unstable" features
#![feature(collections)]

extern crate iron;
extern crate mysql;
extern crate rustc_serialize;
extern crate time;

use rustc_serialize::json;

use std::io::Read;
use iron::method;
use iron::prelude::*;
use iron::status;
use mysql::value::*;
use mysql::conn::*;

#[derive(RustcDecodable, RustcEncodable)]
pub struct Simulacao  {
    id: u64,
    nome: String,
    email: String,
    telefone: String,
    observacoes: String,
}


fn main() {

	let opts = MyOpts {
    user: Some("root".to_string()),
    pass: Some("legisla".to_string()),
    db_name: Some("rust_ws".to_string()),
    ..Default::default()
	};

	let pool = pool::MyPool::new(opts).unwrap();

	//Define server handler as a closure to capture the pool variable
    let hello_world = move |req: &mut Request|  {
    	if req.method == method::Method::Get
    	{
    		return Ok(Response::with((status::Ok, "Wrong method")));
    	}

    	let mut body = String::new();
    	let ret = req.body.read_to_string(&mut body);

        println!("{}",
        match ret {
        Ok(_) => String::from_str("Success"),
        Err(e)  => format!("Error reading POST body: {}", e)
        }
        );
        //Decode JSON to Simulacao struct
    	let decoded: Simulacao = json::decode(&body).unwrap();
        let time_now = time::now();
        let time_string =  time::strftime("%Y-%m-%dT%H:%M:%S%z", &time_now).unwrap();

    	//MySQL query
    	let mut stmt = pool.prepare("INSERT INTO simulacoes (Nome, Email, Telefone, ReceivedTS) VALUES(?, ?, ?, ?)").unwrap();
        let result = stmt.execute(&[&(decoded.nome.to_value()), &(decoded.email.to_value()),
         &(decoded.telefone.to_value()), &(time_string.to_value())]);

        match result {
        // The division was valid
        Ok(_) => println!("Success!"),
        // The division was invalid
        Err(e)  => println!("Mysql Statement returned Error: {}", e)
        }

        let response = "OK\n";
        Ok(Response::with((status::Ok, response)))
    };

    Iron::new(hello_world).http("localhost:3000").unwrap();
}